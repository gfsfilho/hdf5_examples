/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This example illustrates how to create an attribute attached to a
 *  dataset.  It is used in the HDF5 Tutorial.
 */

#include "hdf5.h"
#define FILE "dset.h5"

int main() {

   hid_t       file_id, dataset_id, attribute_id, dataspace_id;
   hsize_t     dims;
   int         attr_data[2];
   herr_t      status;

   /* Inicializa os dados do attribute. */
   attr_data[0] = 100;
   attr_data[1] = 200;

   /* Abre um arquivo existente. */
   file_id = H5Fopen(FILE, H5F_ACC_RDWR, H5P_DEFAULT);
   printf("Arquivo aberto com sucesso!\n");

   /* Abre um dataset existente. */
   dataset_id = H5Dopen(file_id, "/dset", H5P_DEFAULT);
   printf("Dataset aberto com sucesso!\n");

   /* Cria o dataspace do attribute. */
   dims = 2;
   dataspace_id = H5Screate_simple(1, &dims, NULL);
   printf("Dataspace criado com sucesso!\n");

   /* Cria o attribute do dataset. */
   attribute_id = H5Acreate(dataset_id, "Units", H5T_STD_I32BE, dataspace_id, 
                            H5P_DEFAULT, H5P_DEFAULT);
   printf("Attribute criado com sucesso!\n");

   /* Escreves os dados do attribute. */
   status = H5Awrite(attribute_id, H5T_NATIVE_INT, attr_data);
   printf("Dados escritos com sucesso!\n");

   /* Fecha o attribute. */
   status = H5Aclose(attribute_id);
   printf("Attribute fechado!\n");

   /* Fecha o dataspace. */
   status = H5Sclose(dataspace_id);
   printf("dataspace fechado!\n");

   /* Fecha o dataset. */
   status = H5Dclose(dataset_id);
   printf("Dataset fechado!\n");

   /* Fecha o arquivo. */
   status = H5Fclose(file_id);
   printf("Arquivo fechado!\n");
   
   return 0;
}
