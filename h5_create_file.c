# include <stdio.h>
# include "hdf5.h"

int main() {

	hid_t file_id; // identificador do arquivo
	herr_t status; // valores de retorno

	// cria novo arquivo utilizando propriedades default
	file_id = H5Fcreate("file.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    printf("Arquivo criado com sucesso: ");
    puts("file.h5");
	
	// termina o acesso ao arquivo
	status = H5Fclose(file_id);
    printf("Arquivo fechado!\n");
}
