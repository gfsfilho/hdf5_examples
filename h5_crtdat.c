/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This example illustrates how to create a dataset that is a 4 x 6 
 *  array.  It is used in the HDF5 Tutorial.
 */

#include <stdio.h>
#include "hdf5.h"
#define FILE "dset.h5"

int main() {

   hid_t       file_id, dataset_id, dataspace_id;  /* identificadores */
   hsize_t     dims[2];
   herr_t      status;

   /* Cria um novo arquivo utilizando propriedades padrão. */
   file_id = H5Fcreate(FILE, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
   printf("Arquivo criado com sucesso: ");
   puts(FILE);

   /* Cria o dataspace para o dataset. */
   dims[0] = 4; 
   dims[1] = 6; 
   dataspace_id = H5Screate_simple(2, dims, NULL);
   printf("Dataspace %d x %d criado com sucesso!\n", 4, 6);

   /* Cria o dataset. */
   dataset_id = H5Dcreate(file_id, "/dset", H5T_STD_I32BE, dataspace_id, 
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
   printf("Dataset criado com sucesso!\n");

   /* Finaliza o acesso ao dataset e libera seus recursos. */
   status = H5Dclose(dataset_id);
   printf("Dataset fechado!\n");

   /* Termina o acesso ao dataspace. */ 
   status = H5Sclose(dataspace_id);
   printf("Dataspace fechado!\n");

   /* Fecha o arquivo. */
   status = H5Fclose(file_id);
   printf("Arquivo fechado!\n");
   
   return 0;
}
