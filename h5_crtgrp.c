/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright by The HDF Group.                                               *
 * Copyright by the Board of Trustees of the University of Illinois.         *
 * All rights reserved.                                                      *
 *                                                                           *
 * This file is part of HDF5.  The full HDF5 copyright notice, including     *
 * terms governing use, modification, and redistribution, is contained in    *
 * the COPYING file, which can be found at the root of the source code       *
 * distribution tree, or in https://support.hdfgroup.org/ftp/HDF5/releases.  *
 * If you do not have access to either file, you may request a copy from     *
 * help@hdfgroup.org.                                                        *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/*
 *  This example illustrates how to create and close a group. 
 *  It is used in the HDF5 Tutorial.
 */

#include "hdf5.h"
#define FILE "dset.h5"

int main() {

   hid_t       file_id, group_id;
   herr_t      status;
   
   /* Abre um arquivo existente. */
   file_id = H5Fopen(FILE, H5F_ACC_RDWR, H5P_DEFAULT);
   printf("Arquivo aberto com sucesso!\n");

   /* Cria um novo group. */
   group_id = H5Gcreate(file_id, "/MyGroup", H5P_DEFAULT, H5P_DEFAULT,
                        H5P_DEFAULT);
   printf("Group criado com sucesso!\n");
   
   /* Fecha o group. */
   status = H5Gclose(group_id);
   printf("Group fechado!\n");

   /* Termina o acesso ao arquivo. */
   status = H5Fclose(file_id);
   printf("Arquivo fechado!\n");
   
   return 0;
}
