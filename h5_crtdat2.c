# include <stdlib.h>
# include <stdio.h>
# include "hdf5.h"

int main() {

	hid_t       file_id, dataset_id, dataspace_id; 
	hsize_t     dims;
	herr_t      status;
    int *data_buff = malloc(sizeof(int) * 10000000);
	int i;

	for (i = 0; i < 10000000; ++i) {
     
        data_buff[i] = i;
    }
    
    //
    // ESCREVE data_buff EM ARQUIVO HDF5
    //
    file_id = H5Fcreate("dset_test.h5", H5F_ACC_TRUNC, 
                        H5P_DEFAULT, H5P_DEFAULT);
 
	dims = 10000000;
	dataspace_id = H5Screate_simple(1, &dims, NULL);
     
	dataset_id = H5Dcreate(file_id, "/dset", H5T_STD_I32BE, dataspace_id, 
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

	status = H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL,
					 H5P_DEFAULT, data_buff);

	status = H5Dclose(dataset_id);

	status = H5Sclose(dataspace_id);

	status = H5Fclose(file_id);
    
    printf("Escrito em HDF5: dset_test.h5\n");
    
    //
    // ESCREVE data_buff EM ARQUIVO TXT
    //
    FILE *fp;
    fp = fopen("dset_test.txt", "w");
    for(i = 0; i < 10000000; ++i) {
     
        fprintf(fp, "%d\n", data_buff[i]);
    }
    fclose(fp);
    
    printf("Escrito em TXT: dset_test.txt\n");
    
    free(data_buff);

	return 0;
}

